# How to run the project vanilla
- install node module
- install npm
- run 'npm i'
- run 'npm run dev'
- follow the instructions on the console

# How to run the project with Docker
- install docker
- run 'docker-compose up'
- port 3000 must be free. Else change it in docker-compose.yml

Any issue ?
Contact maxence1212@hotmail.fr
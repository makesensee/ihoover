FROM node:18-alpine

WORKDIR /ihoover

RUN apk update && apk upgrade
RUN apk add git

COPY ./package*.json /ihoover/

RUN npm install && npm cache clean --force

COPY . .

ENV PATH ./node_modules/.bin/:$PATH
CMD [ "npm", "run", "dev" ]